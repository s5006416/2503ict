<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/users.php";


// Display search form
Route::get('/', function()
{
	return View::make('users.query');
});

// Perform search and display results
Route::get('search', function()
{
  $searchstr = Input::get('searchstr');
  $results = search($searchstr);

	return View::make('users.results')->withUsers($results)->withSearchstr($searchstr);
});


/* Functions for User database example. */

/* Search sample data for $name or $year or $state from form. */
function search($searchstr) {
  $users = getUsers();
  // Filter users
  if (!empty($users)) {
    $results = array();
    foreach ($users as $user) {
      if ((stripos($user['name'], $searchstr) !== FALSE) ||
        (stripos($user['address'], $searchstr) !== FALSE) ||
        (stripos($user['phone'], $searchstr) !== FALSE) ||
        (stripos($user['email'], $searchstr) !== FALSE) )
      {
        $results[] = $user;
      }
    }
    return $results;
  }
  return $users;
}