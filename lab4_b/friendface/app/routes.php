<?php

require_once ('controllers/posts.php');
require_once ('controllers/friends.php');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	$posts = getPosts();
	$user = getUser();
	$friends = getFriends();
	return View::make('posts')->withPosts($posts)->withUser($user)->withFriends($friends);
});

Route::resource('tweets', 'TweetsController');

Route::get('friends', function()
{
	$posts = getPosts();
	$user = getUser();
	$friends = getFriends();
	return View::make('friends')->withPosts($posts)->withUser($user)->withFriends($friends);
});