<?php
require('friends.php');


function getPosts()
{


    global $user, $koopa, $peach, $mario, $luigi;

    $posts = [
        ['from' => $user, 'to' => $peach, 'message' => "I'm going to kidnap you Peach!"],
        ['from' => $user, 'to' => $mario, 'message' => "Don't even bother trying to stop me Mario!"],
        ['from' => $user, 'to' => $luigi, 'message' => "Tell Mario to stay out of this."],
        ['from' => $user, 'to' => $koopa, 'message' => "Hey Koopa, I need some help with a kidnapping!"],
    ];
    

    $numposts = rand(1,10);
    $retval = array();
    for ($i = 0; $i < $numposts; $i++)
    {
        $retval[] = $posts[$i % count($posts)];
    }

    return $retval;
}
?>