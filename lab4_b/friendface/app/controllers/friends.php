<?php

global $user, $koopa, $peach, $mario, $luigi;
$user = ['id' => 1, 'firstName' => "Bowser", 'lastName' => "Bowser", 'avatar' => "/images/bowser.jpg"];
$koopa = ['id' => 2, 'firstName' => "Koopa", 'lastName' => "Troopa", 'avatar' => "/images/koopatroopa.jpg"];
$peach = ['id' => 3, 'firstName' => "Princess", 'lastName' => "Peach", 'avatar' => "/images/peach.jpg"];
$mario = ['id' => 4, 'firstName' => "Mario", 'lastName' => "Mario", 'avatar' => "/images/mario.png"];
$luigi = ['id' => 5, 'firstName' => "Luigi", 'lastName' => "Luigi", 'avatar' => "/images/luigi.png"];

function getFriends()
{
    global $koopa, $peach, $mario, $luigi;
    
    $friends = [$koopa, $peach, $mario, $luigi];
    return $friends;
}

function getUser()
{
    global $user;
    return $user;
}

?>