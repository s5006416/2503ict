@extends("layouts.master")

@section('content')
<!-- your posts -->
<div class="col-sm-8">
    <h2>{{{$user["firstName"]}}}</h2>
    <p>{{{ count($posts) }}} posts today.</p>
    @foreach($posts as $thispost)
    <div class="panel">
        <div class="panel-body">
            <img src="{{{$thispost["from"]["avatar"]}}}" class="img-responsive img-circle pull-left" alt="user avatar" height="50" width="50">
            <p>{{{ $thispost["message"] }}}</p>
            <img src="{{{$thispost["to"]["avatar"]}}}" class="img-responsive img-circle pull-right" alt="user avatar" height="50" width="50">
        </div>
    </div>
    @endforeach
</div>
@stop