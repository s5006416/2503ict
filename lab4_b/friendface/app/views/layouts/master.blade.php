<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mark Pointing 2503ICT</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">FriendFace</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Find Friends</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/friends">Friends</a></li>
                <li><a href="/">Posts</a></li>
                <li><a href="#">Login</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">

    <!-- friends sidebar -->
    <div class="col-sm-4">
        @foreach($friends as $friend)
        <div class="thumbnail clearfix">
            <img src="<?= $friend["avatar"]?>" class="img-responsive img-circle pull-left" alt="user avatar" height="50" width="50">
            <div class="caption" class="pull-right">
                <a href="/index.php?messageRecipient=<?=$friend["id"]?>" <h3><?= $friend["firstName"]?></h3></a>
            </div>

            @if(array_key_exists('messageRecipient', $_GET))
            @if ($_GET['messageRecipient'] == $friend["id"])
                <form role = "form">
                    <div class = "form-group">
                        <label for = "name">Message <?= $friend['firstName']?></label>
                        <textarea class = "form-control" rows = "3"></textarea>
                    </div>
                </form>
            @endif
            @endif

        </div>
        @endforeach
    </div>

   @yield('content')

</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/2503ict/js/bootstrap.min.js"></script>
</body>
</html>