@extends("layouts.master")

@section('content')
        <h2>Your Friends</h2>
@foreach($friends as $friend)
        <img src="<?= $friend["avatar"]?>" class="img-responsive img-circle pull-left" alt="user avatar" height="50" width="50">
        <div class="caption" class="pull-right">
            <a href="#"> <h3><?= $friend["firstName"]?></h3></a>
        </div>
@endforeach
@stop