@extends ("product.layout")
@section ("content")
<div row>
    <div col-md4></div>

    <div col-md4>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <th scope="row">{{{$product->id}}}</th>
                        <td>{{{$product->name}}}</td>
                        <td>${{{number_format($product->price,2)}}}</td>
                        <td>{{{$product->created_at}}}</td>
                        <td>{{{$product->updated_at}}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div col-md4></div>
</div>
@stop
@stop