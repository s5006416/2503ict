<?php

class ProductsSeeder extends Seeder
{
    public function run()
    {
        Eloquent::unguard();
        
        $product = new Product;
        $product->name = "Red Mushrooms";
        $product->price = 8.00;
        $product->save();
        
        $product = new Product;
        $product->name = "Green Mushrooms";
        $product->price = 16.00;
        $product->save();
    }
}
