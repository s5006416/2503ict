<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 26/05/16
 * Time: 8:15 PM
 */

class Comment extends Eloquent {

    public static $rules = array(
        'content' => 'required',
    );
    
    // this uses eloquent to extend the model functionality into foreign keys.
    public function post()
    {
        return $this->belongsTo('Post');
    }

    // this uses eloquent to extend the model functionality into foreign keys.
    public function user()
    {
        return $this->belongsTo('User');
    }
    
}
