<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	use UserTrait, RemindableTrait, EloquentTrait;

	// the validation rules for usernames and passwords.
	public static $rules = array(
		'email' => 'min:5|required|email|unique:users',
		'password' => 'min:4|required|same:repeat_password',
        'full_name' => 'required',
        'date_of_birth' => 'required|date',
        'repeat_password' => 'same:password'
	);

    // A constructor to allow image upload
    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('profile_image', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ]
        ]);
        parent::__construct($attributes);
    }
    
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 *
    */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    // this uses eloquent to extend the model functionality into foreign keys.
    public function posts()
    {
        return $this->hasMany('Post');
    }

    // this tells the orm which columns should be interpreted as dates.
    public function getDates(){
        return array('date_of_birth', 'created_at', 'updated_at');
    }

    // friend links
    // this uses eloquent to extend the model functionality into foreign keys.
    public function friends()
    {
        return $this->belongsToMany('User', 'friends', 'user_id', 'friend_id');
    }
}
