<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 26/05/16
 * Time: 8:10 PM
 */

class Post extends Eloquent {

    public static $rules = array(
        'content' => 'required',
        'title' => 'required',
        'privacy' => 'required|integer|min:0|max:2'
    );

    // this uses eloquent to extend the model functionality into foreign keys.
    public function comments(){
        return $this->hasMany('Comment');
    }

    // this uses eloquent to extend the model functionality into foreign keys.
    public function user(){
        return $this->belongsTo('User');
    }
    
}

