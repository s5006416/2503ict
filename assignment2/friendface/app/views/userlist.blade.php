/**
* Created by PhpStorm.
* User: mark
* Date: 29/05/16
* Time: 10:30 AM
*/
@extends("layout")
@section('content')
    <div class="col-md-8 chat-window">
        @if(Session::get('message'))
            <p class="well" style="background-color: chartreuse;"><b>{{{Session::get('message')}}}</b></p>
        @endif
    {{Form::open(array('action' => 'FriendController@index', 'method'=>'GET'))}}
        <div class="input-group" style="padding-bottom:10px;">
            <input id="searchInput" name="searchInput" class="form-control input-sm chat_input" placeholder="Search friends..." type="text">
                        <span class="input-group-btn">
                            <input type="submit" value="Search" class="btn btn-primary btn-sm">
                        </span>
        </div>
        <!--div class="col-md-8 col-xs-8">
        <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> Chat - Miguel</h3>
    </div-->
    {{Form::close()}}

    @foreach ($users as $user)
        <div class="col-md-6">
            <div class="well well-sm" style="min-height:150px;">
                <div class="media">
                    <a class="thumbnail pull-left" href="/user/{{{$user->id}}}">
                        <img class="media-object" src="{{ asset($user->profile_image->url('thumb')) }}">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{{$user->full_name}}}</h4>
                        <p>Age: {{{$user->age}}}</p>
                        <p>
                            <a href="/user/{{{$user->id}}}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-zoom-in"></span> View Profile</a>
                            @if(Auth::check())
                            @if(! $user->isYou)
                                @if($user->isFriend)
                                    {{Form::open(array('action'=>array("FriendController@destroy",$user->id)))}}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    <input type="submit" class="btn btn-xs btn-danger" value="Remove Friend">
                                    {{Form::close()}}
                                @else
                                    {{Form::open(array('action'=>"FriendController@store"))}}
                                    {{Form::hidden('friend_id', $user->id)}}
                                    <input type="submit" class="btn btn-xs btn-primary" value="Add Friend">
                                    {{Form::close()}}
                                @endif
                            @else
                                <p>This is you.</p>
                            @endif
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
@stop
@stop
