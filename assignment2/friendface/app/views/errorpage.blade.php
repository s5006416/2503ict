@extends("layout")
@section('content')

    <div class="row">
        <div class="span12">
            <div class="hero-unit center">
                <h1>{{{$errortitle}}}</h1>
                <br>
                <p>{{{$errordescription}}}</p>
                <a href="{{{$returnurl}}}" class="btn btn-large btn-info">Ok, got it.  I'll try that again.</a>
            </div>
            <br>
        </div>
    </div>
@stop
@stop
