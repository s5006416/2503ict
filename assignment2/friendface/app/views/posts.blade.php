
@extends("layout")

@section('content')



    <div class="col-md-8 chat-window">
        <div class="panel panel-default">
            @if (Auth::check())
            <div class="panel-heading top-bar animated slideInDown">
                <form action="{{{action('PostController@store')}}}" method="post">
                    <div class="input-group" style="padding-bottom:10px;">
                        <input id="titleInput" name="titleInput" class="form-control input-sm chat_input" placeholder="Title your post..." type="text">
                    </div>
                    <div class="input-group" style="padding-bottom:10px;">
                        <input id="messageInput" name="messageInput" class="form-control input-sm chat_input" placeholder="Write your message here..." type="text">
                        <span class="input-group-btn">
                            <input type="submit" value="Submit Post" class="btn btn-primary btn-sm" id="btn-chat">
                        </span>
                    </div>
                    <div class="input-group">
                        <select id="privacyInput" name="privacyInput" class="form-control input-sm chat_input">
                            <option value="1">Only my friends can view this post</option>
                            <option value="0">Anyone can view this post</option>
                            <option value="2">Only I can view this post</option>
                        </select>
                    </div>
                    <!--div class="col-md-8 col-xs-8">
                    <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> Chat - Miguel</h3>
                </div-->
                </form>
            </div>
            @endif
            <div class="panel-body msg_container_base animated slideInUp">
                @foreach($posts as $post)
                <!-- ICON ON RIGHT
                <div class="row msg_container base_sent">
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_sent">
                            <p>{{{$post->content}}}</p>
                            <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-2 avatar">
                        <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                    </div>
                </div>
                -->

                <!-- ICON ON LEFT -->
                <div class="row msg_container base_receive">
                    <div class="col-md-2 col-xs-2 avatar">
                        <!-- img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive " -->
                        <img src="{{ asset($post->user->profile_image->url('medium')) }}" class=" img-responsive ">
                    </div>
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_receive">
                            <b>{{{$post->title}}}</b>
                            <p>{{{$post->content}}}</p>
                            <time datetime="{{{$post->created_at}}}">{{{$post->user->full_name}}} • <span id="datespan{{{$post->id}}}">{{{$post->created_at}}}</span> •
                                @if (count($post->comments) == 1)
                                    {{ link_to_route('comments', count($post->comments).' comment', [ $post->id, ]); }}
                                @elseif (count($post->comments))
                                    {{ link_to_route('comments', count($post->comments).' comments', [ $post->id, ]); }}
                                @elseif (Auth::check())
                                    {{ link_to_route('comments', 'Add comment', [ $post->id, ]); }}
                                @endif
                            </time>

                            @if(Auth::check() && Auth::user()->id == $post->user->id)
                                <a href="{{{url("/confirmdeletepost/".$post->id)}}}" class="pull-right text-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                <span class="pull-right">&nbsp;&nbsp;&nbsp;</span>
                                <a href="{{{url("/post/".$post->id."/edit")}}}" class="pull-right text-info"><i class="glyphicon glyphicon-edit"></i></a>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- This function will format the given date/time and place it in the element with the given id -->
                <script>formatLocalTime("{{{$post->created_at}}}", "datespan{{{$post->id}}}");</script>
                @endforeach
            </div>
            <!--div class="panel-footer">
                <div class="input-group">
                    <input id="btn-input" class="form-control input-sm chat_input" placeholder="Write your message here..." type="text">
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" id="btn-chat">Send</button>
                        </span>
                </div>
            </div-->
        </div>
    </div>

@stop
@stop
