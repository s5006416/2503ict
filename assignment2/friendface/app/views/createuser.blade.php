@extends ("layout")
@section ("content")
    <div class="col-md-8 chat-window">
        <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <b>Create new user</b>
            </div>
            <div class="panel-body" style="padding:20px;">
            {{ Form::open(array('action' => array('UserController@store'), 'files'=>true)) }}
            <div class="form-group">
                {{Form::label('full_name', 'Full Name')}}
                {{Form::text('full_name', Input::old('full_name'), array('placeholder'=>'John Smith'))}}
                <div class="help-block with-errors text-danger">{{$errors->first('full_name')}}</div>
            </div>
                <div class="form-group">
                    {{Form::label('email', 'Email')}}
                    {{Form::email('email', Input::old('email'), array('placeholder'=>'me@example.org'))}}
                    <div class="help-block with-errors text-danger">{{$errors->first('email')}}</div>
                </div>

                <div class="form-group">
                    {{Form::label('date_of_birth', 'Date of Birth')}}
                    {{Form::text('date_of_birth', Input::old('date_of_birth'), array('placeholder'=>'8 November 1979'))}}
                    <div class="help-block with-errors text-danger">{{$errors->first('date_of_birth')}}</div>
                </div>

                <div class="form-group">
                    {{ Form::label('profile_image', 'Profile Image') }}
                    {{ Form::file('profile_image') }}
                    <div class="help-block with-errors text-danger">{{$errors->first('profile_image')}}</div>
                </div>

                <div class="form-group">
                    {{Form::label('password', 'Password')}}
                    {{Form::password('password')}}
                    <div class="help-block with-errors text-danger">{{$errors->first('password')}}</div>
                </div>

                <div class="form-group">
                    {{Form::label('repeat_password', 'Repeat Password')}}
                    {{Form::password('repeat_password')}}
                    <div class="help-block with-errors text-danger">{{$errors->first('repeat_password')}}</div>
                </div>

            {{Form::submit('Submit')}}

            {{Form::close()}}
        </div>
    </div>

@stop
@stop
