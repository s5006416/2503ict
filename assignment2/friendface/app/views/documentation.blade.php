
@extends("layout")

@section('content')

    <div class="col-md-8 chat-window">
        <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <div class="col-md-8 col-xs-8">
                    <h3 class="panel-title" style="padding-bottom: 10px;"><span class="glyphicon glyphicon-book"></span> Documentation</h3>
                </div>
            </div>
            <div class="panel-body doc_container_base">

                <h3>Project Documetation 2503ICT Assignment 2</h3>
                <h4>Mark Pointing S5006416</h4>
                <h4>ER Diagram for the Social Network Database</h4>
                <img src="/images/EER_Diagram.svg" alt="EER Diagram"/>
                <h4>Features Implemented</h4>
                <p>All required features were implemented, including the following:</p>
                <ul>
                    <li>Navigation menu on all pages</li>
                    <li>Create post form on home page</li>
                    <li>Only create post/comment when logged in (with server side checking)</li>
                    <li>Each post contains an image</li>
                    <li>Each post contains a title, message and name</li>
                    <li>Each post indicates the number of comments</li>
                    <li>Each post has edit and delete links, only available to owner, as well as server side validation of ownership</li>
                    <li>Each post has a view comments link which redirects to a new page, comments are paginated</li>
                    <li>Each comment and post can be edited in a form on a new page</li>
                    <li>Each post has privacy settings which can also be edited</li>
                </ul>

                <h4>Challenges and Interesting Solutions</h4>
                    <p>Foreign keys were implemented in the model with hasMany and belongsToMany functions.  Also cascaded deletion of
                    posts and comments was achieved in the schema with onDelete('cascade').</p>

                <h4>Extra Features</h4>
                    <p>All database operations are validated on the server side, even if client side protection is available.</p>
                    <p>All database delete operations redirect to a confirmation page before carrying out the delete operation.
                    This is designed to protect against inadvertent post or comment deletion.  Add and remove friends displays
                    a confirmation message with Session::flash().</p>
            </div>
        </div>
    </div>

@stop
@stop
