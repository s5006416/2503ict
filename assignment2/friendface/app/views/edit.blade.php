
@extends("layout")

@section('content')
    <div class="col-md-8 chat-window">
        <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <div class="col-md-8 col-xs-8" style="padding-bottom: 10px;">
                <h3 class="panel-title"><span class="glyphicon glyphicon-edit"></span> Edit Post</h3>
                </div>

                {{ Form::model($post, array('route' => array('post.update', $post->id), 'method' => 'PUT')) }}
                    <div class="input-group" style="padding-bottom:10px;width:100%;">
                        <label for="titleInput">Title</label>
                        <input id="titleInput" name="titleInput" class="form-control input-sm chat_input" placeholder="Title your post..." type="text" value="{{{$post->title}}}">
                    </div>
                    <div class="input-group" style="padding-bottom:10px;width:100%;">
                        <label for="messageInput">Post</label>
                        <textarea id="messageInput" name="messageInput" class="form-control input-sm chat_input" placeholder="Write your message here...">{{{$post->content}}}</textarea>
                    </div>

                    <div class="input-group" style="padding-bottom: 10px;">
                        <select id="privacyInput" name="privacyInput" class="form-control input-sm chat_input" value="">
                            <option value="1" @if($post->privacy == 1) selected @endif >Only my friends can view this post</option>
                            <option value="0" @if($post->privacy == 0) selected @endif >Anyone can view this post</option>
                            <option value="2" @if($post->privacy == 2) selected @endif >Only I can view this post</option>
                        </select>
                    </div>
                    <div class="input-group">
                        <input type="submit" value="Save Changes" class="btn btn-primary btn-sm" id="buttonSubmit">
                        <a href="{{url("/")}}" class="btn btn-default btn-sm" id="buttonCancel">Discard Changes</a>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@stop
