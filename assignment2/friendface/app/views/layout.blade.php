<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mark Pointing 2503ICT</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <!-- Local Style Sheets -->
    <link href="/css/chatstyle.css" rel="stylesheet">
    <link href="/css/profilestyle.css" rel="stylesheet">
    <link href="/css/loginstyle.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">

    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script>
        // a script to convert UTC from the server to localtime, and push it into an element.
        var formatLocalTime = function(utcTime, elementId){
            utcTime = utcTime.replace(" ", "T");
            utcTime += "+00:00";
            console.log(utcTime);
            var myDate = new Date(utcTime);
            var dateString = myDate.toString();
            var pos = dateString.lastIndexOf(":");
            dateString = dateString.substr(0, pos);
            $('#'+elementId).html(dateString);
        }
    </script>

</head>
<body>

<?php
        $today = new DateTime();
        if(Auth::check()){
            if (empty($sidebar_user)){
                $sidebar_user = Auth::user();
            }
            if($sidebar_user->id == Auth::user()->id) {
                $sidebar_user->status = "(You)";
                $sidebar_user->isYou = true;
                $sidebar_user->isFriend = false;
            }
            else if(Friend::where('user_id',"=",Auth::user()->id)->where('friend_id', "=", $sidebar_user->id)->count() ){
                $sidebar_user->status = "Friend";
                $sidebar_user->isYou = false;
                $sidebar_user->isFriend = true;
            }
            else{
                $sidebar_user->status = "FriendFace User";
                $sidebar_user->isYou = false;
                $sidebar_user->isFriend = false;
            }
        }

        if(!empty($sidebar_user)) $sidebar_user->age = $sidebar_user->date_of_birth->diff($today)->y;

?>

<div class="container">
    <!-- --------------------------------------------- -->
    <div class="col-md-4">
        <div class="row profile">
            <div class="profile-sidebar">
                @if (Auth::check() || !empty($sidebar_user))
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <!-- img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive "-->
                        <img src="{{ asset($sidebar_user->profile_image->url('medium')) }}" class=" img-responsive ">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            {{$sidebar_user->full_name}}
                        </div>

                        @if(! $sidebar_user->isYou)
                            <div class="profile-usertitle-job">
                                {{$sidebar_user->status}}
                            </div>
                        @endif

                        <div class="profile-usertitle-job">
                            {{$sidebar_user->age}} Years old
                        </div>

                        @if(! $sidebar_user->isYou)

                            @if($sidebar_user->isFriend)
                                {{Form::open(array('action'=>array("FriendController@destroy",$sidebar_user->id)))}}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <input type="submit" class="btn btn-xs btn-danger" value="Remove Friend">
                                {{Form::close()}}
                            @else
                                {{Form::open(array('action'=>"FriendController@store"))}}
                                {{Form::hidden('friend_id', $sidebar_user->id)}}
                                <input type="submit" class="btn btn-xs btn-primary" value="Add Friend">
                                {{Form::close()}}
                            @endif
                        @endif

                    </div>


                @else
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img src="/images/friendface_symbol.png" class=" img-responsive ">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            FriendFace
                        </div>
                        <div class="profile-usertitle-job">
                            Welcome to FriendFace
                        </div>
                    </div>
                @endif
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <!-- div class="profile-userbuttons">
                    <button type="button" class="btn btn-success btn-sm">Follow</button>
                    <button type="button" class="btn btn-danger btn-sm">Message</button>
                </div -->
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">

                        <!--li class="active"-->
                        <li class="animated slideInLeft">
                            <a href="/">
                                <i class="glyphicon glyphicon-home"></i>
                                Home </a>
                        </li>

                        <li class="animated slideInLeft">
                            <a href="{{action('UserController@index')}}">
                                <i class="glyphicon glyphicon-search"></i>
                                Find New Friends </a>
                        </li>

                        @if(!empty($sidebar_user))
                        <li class="animated slideInLeft">
                            <a href="{{action('FriendController@show', $sidebar_user->id)}}">
                                <i class="glyphicon glyphicon-user"></i>
                                Show Friends</a>
                        </li>
                        @endif

                            <li class="animated slideInLeft">
                                <a href="{{{url("/documentation")}}}">
                                    <i class="glyphicon glyphicon-book"></i>
                                    Documentation </a>
                            </li>

                        @if (Auth::check())
                            <li class="animated slideInLeft">
                                <a href="{{{route('user.logout', 'Logout')}}}">
                                    <i class="glyphicon glyphicon-log-out"></i>
                                    Log Out</a>
                            </li>

                        @else
                            <li class="dropdown animated slideInLeft">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-log-in"></i><b>Login/Join</b> <span class="caret"></span></a>
                                <ul id="login-dp" class="dropdown-menu">
                                    <li>
                                        <div class="row">
                                            <div class="col-md-12">
                                                {{-- Form::open(array('url' => secure_url('user/login'))) --}}
                                                {{ Form::open(array('url' => url('user/login'))) }}
                                                <div class="form-group">
                                                    {{Form::label('email', 'Email', ["class" => "sr-only"])}}
                                                    {{Form::text('email', "",["class"=>"form-control", "placeholder"=>"Email", "required"=>"" ])}}
                                                </div>
                                                <div class="form-group">
                                                    {{Form::label('password', 'Password', ["class" => "sr-only"])}}
                                                    {{Form::password('password', ["class"=>"form-control", "placeholder"=>"Password", "required"=>"" ])}}
                                                </div>

                                                <div class="form-group">
                                                    {{Form::submit('Sign In', array("class" => "btn btn-primary btn-block"))}}
                                                </div>
                                                {{Form::close()}}
                                            </div>
                                            <div class="bottom text-center">
                                                New here ? {{ link_to_route('user.create', 'Join us') }}
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            @if (Session::get('loginError'))
                                <li>
                                    <a href="#"><i class="glyphicon glyphicon-remove-circle text-danger"></i><b class="text-danger">{{ Session::get('loginError') }}</b></a>
                                </li>
                            @endif

                        @endif

                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>
    </div>
    <!-- --------------------------------------------- -->
    <!-- content -->
    @yield('content')
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</body>
</html>