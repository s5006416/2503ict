
@extends("layout")

@section('content')

    <div class="col-md-8 chat-window">
        <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <div class = "pull-right">{{ $comments->links() }}</div>
                <div class="col-md-8 col-xs-8">
                    <h3 class="panel-title pull-left" style="padding-bottom: 10px;"><span class="glyphicon glyphicon-comment"></span> Comments</h3>
                </div>
                @if(Auth::check())
                <form action="{{{url("/comments")}}}" method="post">
                    <input type="hidden" name="postId" value="{{{$post->id}}}">
                    <div class="input-group" style="margin: 10px; width: 95%">
                        <input id="messageInput" name="messageInput" class="form-control input-sm chat_input" placeholder="Write your comment here..." type="text">
                        <span class="input-group-btn">
                            <input type="submit" class="btn btn-primary btn-sm" id="btn-chat" value="Submit Comment">
                        </span>
                    </div>
                </form>
                @endif
            </div>
            <div class="panel-body msg_container_base">
                <!-- ICON ON LEFT -->
                <div class="row msg_container base_receive">
                    <div class="col-md-2 col-xs-2 avatar">
                        <!-- img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive " -->
                        <img src="{{ asset($post->user->profile_image->url('medium')) }}" class=" img-responsive ">

                    </div>
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_receive">
                            <b>{{{$post->title}}}</b>
                            <p>{{{$post->content}}}</p>
                            <time datetime="2009-11-13T20:00">{{{$post->user->full_name}}} • <span id="datespanPost">{{{$post->created_at}}}</span>
                            </time>
                        </div>
                    </div>
                </div>
                <!-- This function will format the given date/time and place it in the element with the given id -->
                <script>formatLocalTime("{{{$post->created_at}}}", "datespanPost");</script>
                @foreach($comments as $comment)
                        <!-- ICON ON RIGHT -->
                <div class="row msg_container base_sent">
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_sent">
                            <p>{{{$comment->content}}}</p>
                            <time datetime="{{{$comment->created_at}}}">{{{$comment->user->full_name}}} • <span id="datespan{{{$post->id}}}">{{{$comment->created_at}}}</span></time>
                            @if(Auth::check() && Auth::user()->id == $comment->user->id)
                                <a href="{{{url("/confirmdeletecomment/".$comment->id)}}}" class="pull-left text-danger"><i class="glyphicon glyphicon-remove"></i></a>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-2 col-xs-2 avatar">
                        <!-- img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive " -->
                        <img src="{{ asset($comment->user->profile_image->url('medium')) }}" class=" img-responsive ">
                    </div>
                </div>
                <!-- This function will format the given date/time and place it in the element with the given id -->
                <script>formatLocalTime("{{{$post->created_at}}}", "datespan{{{$post->id}}}");</script>
                @endforeach
            </div>
            <!--div class="panel-footer">
                <div class="input-group">
                    <input id="btn-input" class="form-control input-sm chat_input" placeholder="Write your message here..." type="text">
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" id="btn-chat">Send</button>
                        </span>
                </div>
            </div-->
        </div>
    </div>

@stop
@stop
