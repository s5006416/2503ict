<?php

class UserController extends \BaseController {

    public function login(){

        $values = Input::all();
        if (Auth::attempt(array('email' => $values['email'], 'password' => $values['password']))){
            Session::put('loginError', "");
        }
        else {
            Session::put('loginError', "Username or Password Incorrect");
        }
        return Redirect::to(URL::previous());

    }

    public function logout(){
        Auth::logout();
        return Redirect::action("PostController@index");
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$users = User::select("full_name", 'date_of_birth', "profile_image_file_name", "profile_image_file_size", "profile_image_content_type", "profile_image_updated_at")->get();
        $users = User::get();

        // add age field to each user object
        $today = new DateTime();
        foreach ($users as $user)
        {
            $user->age = $user->date_of_birth->diff($today)->y;
            
            if (Auth::check()) {

                if ($user->id == Auth::user()->id) {
                    $user->isYou = true;
                    $user->isFriend = false;
                } else if (Friend::where('user_id', "=", Auth::user()->id)->where('friend_id', "=", $user->id)->count() > 0) {
                    $user->isYou = false;
                    $user->isFriend = true;
                } else {
                    $user->isYou = false;
                    $user->isFriend = false;
                }
            }
            else{
                $user->isYou = false;
                $user->isFriend = false;
            }
            
        }
        return View::make('userlist')->withUsers($users);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('createuser');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $values = Input::all();
        $v = Validator::make($values, User::$rules);
        if ( $v->passes()) {
            $user = new User();
            $user["email"] = $values["email"];
            $user["password"] = Hash::make($values["password"]);
            $user["full_name"] = $values['full_name'];
            $user['date_of_birth'] = new DateTime($values['date_of_birth']);
            $user['profile_image'] = $values['profile_image'];
            $user->save();
            //return Redirect::route("user.show", $user->id);
            Auth::attempt(array('email' => $values['email'], 'password' => $values['password']));
            return Redirect::route('post.index');
        }
        else
        {
            // Show validation errors
            return Redirect::action('UserController@create')->withErrors($v)->withInput(Input::except('password'));
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $sidebar_user = User::find($id);

        // get all entries reverse ordered by date
        if(Auth::check())
        {
            $posts = Post::where('user_id','=', $id)->where('privacy', '=', 1)->whereIn("user_id", Friend::where('friend_id', '=', Auth::user()->id )->lists('user_id'))
                ->orWhere('privacy', '<', 1)->where('user_id','=', $id)->orWhere('user_id', '=', $id)->where('user_id','=', Auth::user()->id)->orderBy('created_at', 'DEC')->get();
        }
        else
        {
            $posts = Post::where('user_id','=', $id)->where('privacy', '=', 0)->orderBy('created_at', 'DEC')->get();
        }
        
        // render view
        return View::make('posts')->withPosts($posts)->withSidebar_user($sidebar_user);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
