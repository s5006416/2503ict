<?php

class FriendController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$search = Input::get('searchInput');
		$users = User::where('full_name', 'like', '%'.$search.'%')->get();
		// add age field to each user object
		$today = new DateTime();
		foreach ($users as $user)
		{
			$user->age = $user->date_of_birth->diff($today)->y;
			// if logged in, add the required isYou and isFriend properties.
			if (Auth::check()) {

				if ($user->id == Auth::user()->id) {
					$user->isYou = true;
					$user->isFriend = false;
				} else if (Friend::where('user_id', "=", Auth::user()->id)->where('friend_id', "=", $user->id)->count() > 0) {
					$user->isYou = false;
					$user->isFriend = true;
				} else {
					$user->isYou = false;
					$user->isFriend = false;
				}
			}
			else{
				$user->isYou = false;
				$user->isFriend = false;
			}

		}
		return View::make('userlist')->withUsers($users);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		echo("FriendController@create");
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$friend_id = Input::get("friend_id");

		// server side check to make sure we are logged in.
		if (! Auth::check())
		{
			return View::make("errorpage")->withErrortitle("You're not logged in!")->withErrordescription("Only logged in users are allowed to add friends. Please log in or join before adding friends.")->withReturnurl("/user");
		}

		// check that this friend is not already in the database
		if(Friend::where('user_id',"=",Auth::user()->id)->where('friend_id', "=", $friend_id)->count() > 0 ){
			// they are already friends, the UI does not allow this, so it's probably a hack.  Don't bother with
			// an error message.  Just redirect back to users page
			return Redirect::back();
		}
		
		// check that the friend is a valid user
		if (User::where('id', '=',$friend_id)->count() == 0)
		{
			// the ui does not allow this so must be a hack.  no error message displayed.
			return Redirect::back();
		}
		
		$f = new Friend();
		$f->user_id = Auth::user()->id;
		$f->friend_id = $friend_id;
		$f->save();
		
		// save the inverse relationship.  In real life this would be done
		// when a friend request is accepted
		$f = new Friend();
		$f->friend_id = Auth::user()->id;
		$f->user_id = $friend_id;
		$f->save();

		Session::flash('message', 'Successfully added friend!');
		return Redirect::back();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$users = User::whereIn("id", Friend::where('friend_id', '=', $id )->lists('user_id'))->get();

		// add age field to each user object
		$today = new DateTime();
		foreach ($users as $user)
		{
			$user->age = $user->date_of_birth->diff($today)->y;

			// if logged in, add the required isYou and isFriend properties.
			if (Auth::check()) {

				if ($user->id == Auth::user()->id) {
					$user->isYou = true;
					$user->isFriend = false;
				} else if (Friend::where('user_id', "=", Auth::user()->id)->where('friend_id', "=", $user->id)->count() > 0) {
					$user->isYou = false;
					$user->isFriend = true;
				} else {
					$user->isYou = false;
					$user->isFriend = false;
				}
			}
			else{
				$user->isYou = false;
				$user->isFriend = false;
			}

		}
		return View::make('userlist')->withUsers($users)->withSidebar_user(User::find($id));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// just for debugging incorrect routing.
		echo("FriendController@edit");
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// just for debugging incorrect routing.
		echo("FriendController@update");
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$friend_id = $id;

		// server side check to make sure we are logged in.
		if (! Auth::check())
		{
			return View::make("errorpage")->withErrortitle("You're not logged in!")->withErrordescription("Only logged in users are allowed to post on this site. Please log in or join before posting.")->withReturnurl("/user");
		}

		// this will only delete what's already in the database
		$entries = Friend::where('user_id',"=",Auth::user()->id)->where('friend_id', "=", $friend_id)->get();
		echo($entries->count());
        foreach($entries as $entry){
            $entry->delete();
        }

		// this will only delete what's already in the database (inverse relationship)
		$entries = Friend::where('friend_id',"=",Auth::user()->id)->where('user_id', "=", $friend_id)->get();
		echo($entries->count());
		foreach($entries as $entry){
			$entry->delete();
		}
		
		// display confirmation message
		Session::flash('message', 'Successfully removed friend!');
		return Redirect::back();

	}


}
