<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// get all entries reverse ordered by date
		if(Auth::check())
        {
            $posts = Post::where('privacy', '=', 1)->whereIn("user_id", Friend::where('friend_id', '=', Auth::user()->id )->lists('user_id'))->orWhere('privacy', '<', 1)->orWhere('user_id', '=', Auth::user()->id)->orderBy('created_at', 'DEC')->get();
        }
        else
        {
            $posts = Post::where('privacy', '=', 0)->orderBy('created_at', 'DEC')->get();

        }


		// render view
		return View::make('posts')->withPosts($posts);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($args)
	{
        echo("Post.create");
		// Not required.
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        //server side check that the user is logged in.
		if (! Auth::check()) {
			return View::make("errorpage")->withErrortitle("You're not logged in!")->withErrordescription("Only logged in users are allowed to post on this site. Please log in or join before posting.")->withReturnurl("/");
		}
		// read the query parameters
		$user_id = Auth::user()->id;
		$title = $_POST["titleInput"];
		$content = $_POST["messageInput"];
		$privacy = $_POST['privacyInput'];
		
		// redirect to an error page if any field is empty
        $values = compact('title', 'content', 'privacy');
        $v = Validator::make($values, Comment::$rules);
        if ( ! $v->passes()) {
			return View::make('errorpage')->withErrortitle("There was a problem with your post!")->withErrordescription('You need to enter a title, and message!')->withReturnurl(url("/"));
		}

		// perform the actual DB insertion
		$p = new Post();
			
		//'user_id' => $user_id,
		$p->title = $title;
		$p->content = $content;
		$p->privacy = $privacy;
		$p->user_id = $user_id;
		//$p->user()->associate(Auth::user());

		$p->save();

		// return to home page
		return Redirect::to(url("/"));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        echo("Post.show");
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $post = Post::find($id);
        return View::make('edit')->withPost($post);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $title = Input::get("titleInput");
        $content = Input::get("messageInput");
        $privacy = Input::get("privacyInput");

        // redirect to an error page if any field is empty
        $values = compact('title', 'content', 'privacy');
        $v = Validator::make($values, Post::$rules);
        if ( ! $v->passes()) {
            return View::make('errorpage')->withErrortitle("There was a problem with your post!")->withErrordescription('You need to make sure title, and message are not blank!')->withReturnurl(url("/post/".$id."/edit"));
        }

        // populate and save the model
        $p = Post::find($id);
        $p->title = $title;
        $p->content = $content;
        $p->privacy = $privacy;
        $p->save();
        
        return Redirect::to(url("/"));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // server side check that the user is logged in.
		if (! Auth::check())
		{
			return View::make("errorpage")->withErrortitle("You're not logged in!")->withErrordescription("Only logged in users are allowed to post on this site. Please log in or join before posting.")->withReturnurl("/");
		}

        // sever side check that the user owns the post.
		if (Auth::user()->id != Post::find($id)->user_id)
		{
			return View::make("errorpage")->withErrortitle("That's not your post!")->withErrordescription("You can only delete posts which you created.")->withReturnurl("/");
		}

        //NOTE: Comment deletion is taken care of via cascaded delete.
		Post::destroy($id);
		
		return Redirect::to(action("PostController@index"));
	}


}
