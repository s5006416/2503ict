<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($postId)
	{
		$post = Post::find($postId);
		$comments = Comment::where('post_id', '=', $postId)->paginate(4);
		return View::make('comments')->withPost($post)->withComments($comments);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//not required
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// only allow registered users to comment
		if (Auth::check()) {
			// read the query parameters
			$message = Input::get("messageInput");
			$postId = Input::get("postId");

			// redirect to an error page if the message is empty
			$values = array('content' => $message);
			$v = Validator::make($values, Comment::$rules);
			if ( ! $v->passes()) {
				return View::make('errorpage')
					->withErrortitle("There was a problem with your comment!")
					->withErrordescription('You need to enter a message!')
					->withReturnurl(url("/comments/" . $postId));
			}

			// perform the actual db insertion
			$c = new Comment();

			$c->post_id = $postId;
			$c->user_id = Auth::user()->id;
			$c->content = $message;
			$c->save();
		}
		// redirect to comments page.
		return Redirect::to(url("/comments/".$postId));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		echo ("Comments.show");
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$postid = Comment::find($id)->post->id;
		
		// server side check for authorised user
		if (! Auth::check())
		{
			return View::make("errorpage")->withErrortitle("You're not logged in!")->withErrordescription("Only logged in users are allowed to post on this site. Please log in or join before posting.")->withReturnurl("/comments/".$postid);
		}

		// server side check for ownership of comment to be deleted.
		if (Auth::user()->id != Comment::find($id)->user_id)
		{
			return View::make("errorpage")->withErrortitle("That's not your comment!")->withErrordescription("You can only delete comments which you created.")->withReturnurl("/comments/".$postid);
		}
		
		
		Comment::destroy($id);

		return Redirect::to(action("CommentController@index",array($postid)));
	}

}
