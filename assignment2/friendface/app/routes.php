<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* This is the root route.  It simply renders the home page using a query of all entries from
the posts database, mashed with matching comments for the purposes of a count only*/
Route::get('/', array('as' => 'post.index', 'uses' => 'PostController@index'));

/* This route gets all the comments associated with a given post
and renders the comments view */
Route::get('/comments/{postId}', array('as'=>"comments", 'uses' => 'CommentController@index'));
Route::get('/comments/delete/{id}', array('as'=>"comments.delete", 'uses' => 'CommentController@destroy'));
Route::resource('comments', 'CommentController');

Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
Route::resource('user', 'UserController');
Route::get('/post/delete/{id}', array('as' => 'post.delete', 'uses' => 'PostController@destroy'));
Route::resource('post', 'PostController');

Route::resource('friend', 'FriendController');


/* This route redirects to the confirmation page.  It is used as confirmation of delete, which then
redirects to the route which actually performs the redirect.*/
Route::get('/confirmdeletepost/{id}', array('as'=>"confirmdeletepost", function($id)
{
    // get the post in question so that we can display some information about it on the confirmation page
    $post = Post::find($id);
    return View::make('confirm')
        ->withTitle("Are you sure you want to delete this post?")
        ->withDescription($post->user->full_name.': '.$post->content)
        ->withConfirmurl(url('/post/delete/'.$post->id))
        ->withCancelurl(url('/'))
        ->withOklabel('Delete')
        ->withCancellabel('Cancel');
}));

/* This route redirects to the confirmation page for comment deletion,
the actual deletion is done on another page */
Route::get('/confirmdeletecomment/{id}', array('as'=>"confirmdeletecomment", function($id)
{
    // get the comment so we can display some information about it on the confirmation page.
    $comment = Comment::find($id);
    return View::make('confirm')
        ->withTitle("Are you sure you want to delete this comment?")
        ->withDescription($comment->user->full_name.': '.$comment->content)
        ->withConfirmurl(url('/comments/delete/'.($comment->id)))
        ->withCancelurl(url('/comments/'.$comment->post->id))
        ->withOklabel('Delete')
        ->withCancellabel('Cancel');
}));

/* Route to display the documentation */
Route::get('/documentation', array('as'=>"documentation", function()
{
    return View::make('documentation');
}));