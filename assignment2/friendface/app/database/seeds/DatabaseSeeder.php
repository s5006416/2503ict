<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
        $this->call('PostsTableSeeder');
        $this->call('CommentsTableSeeder');
        $this->call('FriendsTableSeeder');
        $this->call('ComplianceSeeder');
	}

}

class UserTableSeeder extends Seeder {

    public function run(){
        Eloquent:: unguard();
        DB::table('users')->delete();
        User::create(array(
            'id' => 0,
            'email' => 'bob@a.org',
            'full_name' => 'Bob',
            'date_of_birth' => '1981-10-07',
            'password' => Hash::make('1234')
        ));


        User::create(array(
            'id' => 1,
            'email' => 'john@a.org',
            'full_name' => 'John',
            'date_of_birth' => '1979-11-08',
            'password' => Hash::make('1234')
        ));

        User::create(array(
            'id' => 2,
            'email' => 'tom@a.org',
            'full_name' => 'Tom',
            'date_of_birth' => '1979-11-08',
            'password' => Hash::make('1234')
        ));

        User::create(array(
            'id' => 3,
            'email' => 'mario@mariobros.org',
            'full_name' => 'Mario',
            'date_of_birth' => '1980-01-01',
            'password' => Hash::make('1234')
        ));

        User::create(array(
            'id' => 4,
            'email' => 'luigi@mariobros.org',
            'full_name' => 'Luigi',
            'date_of_birth' => '1980-01-01',
            'password' => Hash::make('1234')
        ));

        User::create(array(
            'id' => 5,
            'email' => 'princess@mariobros.org',
            'full_name' => 'Princess Peach',
            'date_of_birth' => '1980-01-01',
            'password' => Hash::make('1234')
        ));

        User::create(array(
            'id' => 6,
            'email' => 'bowser@mariobros.org',
            'full_name' => 'Bowser',
            'date_of_birth' => '1980-01-01',
            'password' => Hash::make('1234')
        ));

        User::create(array(
            'id' => 7,
            'email' => 'koopa@mariobros.org',
            'full_name' => 'Koopa Troopa',
            'date_of_birth' => '1980-01-01',
            'password' => Hash::make('1234')
        ));

        User::create(array(
            'id' => 8,
            'email' => 'yoshi@mariobros.org',
            'full_name' => 'Yoshi',
            'date_of_birth' => '1980-01-01',
            'password' => Hash::make('1234')
        ));

        User::create(array(
            'id' => 9,
            'email' => 'toad@mariobros.org',
            'full_name' => 'Toad',
            'date_of_birth' => '1980-01-01',
            'password' => Hash::make('1234')
        ));

    }
}

class PostsTableSeeder extends Seeder {

    public function run(){
        Eloquent:: unguard();
        
        DB::table('posts')->delete();

        Post::create(array(
            'user_id'=>DB::table('users')->where('full_name', 'like', 'Bob')->pluck('id'),
            'title' => "Bob's Post 1",
            'content' => "Bob's public post.",
            'privacy' => 0
        ));

        Post::create(array(
            'user_id'=>DB::table('users')->where('full_name', 'like', 'Bob')->pluck('id'),
            'title' => "Bob's Post 2",
            'content' => "Bob's friend post.",
            'privacy' => 1
        ));

        Post::create(array(
            'user_id'=>DB::table('users')->where('full_name', 'like', 'Bob')->pluck('id'),
            'title' => "Bob's Post 3",
            'content' => "Bob's private post.",
            'privacy' => 2
        ));


    }
}

class CommentsTableSeeder extends Seeder {

    public function run(){
        Eloquent:: unguard();

        DB::table('comments')->delete();
        
        Comment::create(array(
            'post_id' => DB::table('posts')->where('title', 'like', 'Bob%')->pluck('id'),
            'user_id'=>DB::table('users')->where('full_name', 'like', 'Bob')->pluck('id'),
            'content'=>"My posts are stupid."
        ));
        
        
        
    }
}

class FriendsTableSeeder extends Seeder {

    public function run(){
        Eloquent:: unguard();

        DB::table('friends')->delete();
        
        Friend::create(array(
            'user_id'=>DB::table('users')->where('full_name', 'like', 'Bob')->pluck('id'),
            'friend_id'=>DB::table('users')->where('full_name', 'John')->pluck('id'),
        ));

        Friend::create(array(
            'user_id'=>DB::table('users')->where('full_name', 'like', 'John')->pluck('id'),
            'friend_id'=>DB::table('users')->where('full_name', 'like', 'Bob')->pluck('id'),
        ));

    }
}

class ComplianceSeeder extends Seeder {
    // this adds all the extra stuff required for compliance with the assignment specification.
    public function run(){
        Eloquent:: unguard();

        $user_id = DB::table('users')->where('full_name', 'like', 'John')->pluck('id');

        $arr=["Public", 'Friend', 'Private'];
        for($i=0; $i<10; $i++) {
            Post::create(array(
                'user_id' => $user_id,
                'title' => "John's Post ".$i." (".$arr[$i%3].")",
                'content' => "I have posted ".$i." times!",
                'privacy' => $i %  3
            ));
        }

        $post_id = Post::where('user_id', '=', $user_id)->pluck('id');
        $user_id = DB::table('users')->where('full_name', 'like', 'Bob')->pluck('id');

        for($i=0; $i<10; $i++) {
            Comment::create(array(
                'user_id' => $user_id,
                'post_id' => $post_id,
                'content' => "I have commented ".$i." times!",
            ));
        }



    }
}