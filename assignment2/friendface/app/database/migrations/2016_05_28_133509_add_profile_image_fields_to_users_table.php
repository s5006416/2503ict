<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddProfileImageFieldsToUsersTable extends Migration {

    /**
     * Make changes to the table.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {

            $table->string('profile_image_file_name')->nullable();
            $table->integer('profile_image_file_size')->nullable()->after('profile_image_file_name');
            $table->string('profile_image_content_type')->nullable()->after('profile_image_file_size');
            $table->timestamp('profile_image_updated_at')->nullable()->after('profile_image_content_type');

        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {

            $table->dropColumn('profile_image_file_name');
            $table->dropColumn('profile_image_file_size');
            $table->dropColumn('profile_image_content_type');
            $table->dropColumn('profile_image_updated_at');

        });
    }

}