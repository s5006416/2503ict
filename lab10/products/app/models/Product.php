<?php

class Product extends Eloquent{
    public static $rules = array(
        'name' => 'min:5|required',
        'price' => 'required'
    );

}
