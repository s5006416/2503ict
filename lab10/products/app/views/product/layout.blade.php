<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mark Pointing 2503ICT</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <!-- Local Style Sheets -->
    <link href="/css/bodystyle.css" rel="stylesheet">
    <link href="/css/loginstyle.css" rel="stylesheet">

    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script>
        // a script to convert UTC from the server to localtime, and push it into an element.
        var formatLocalTime = function(utcTime, elementId){
            utcTime = utcTime.replace(" ", "T");
            utcTime += "+00:00";
            console.log(utcTime);
            var myDate = new Date(utcTime);
            var dateString = myDate.toString();
            var pos = dateString.lastIndexOf(":");
            dateString = dateString.substr(0, pos);
            $('#'+elementId).html(dateString);
        }
    </script>

</head>
<body >

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<nav class="navbar navbar-default navbar-inverse" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Products</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      </ul>
      <ul class="nav navbar-nav navbar-right">
          @if (Auth::check())
              {{ Auth::user()->username }}

              <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>{{ Auth::user()->username }}</b> <span class="caret"></span></a>
                  <ul id="login-dp" class="dropdown-menu">
                      <li>
                          <div class="row">
                              <div class="col-md-12">
                                  {{ link_to_route('user.logout', 'Logout') }}
                              </div>
                          </div>
                      </li>
                  </ul>
              </li>


          @else

          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
              <ul id="login-dp" class="dropdown-menu">
                  <li>
                      <div class="row">
                          <div class="col-md-12">
                              {{-- Form::open(array('url' => secure_url('user/login'))) --}}
                              {{ Form::open(array('url' => url('user/login'))) }}
                              <div class="form-group">
                                  {{Form::label('username', 'Username', ["class" => "sr-only"])}}
                                  {{Form::text('username', "",["class"=>"form-control", "placeholder"=>"Username", "required"=>"" ])}}
                              </div>
                              <div class="form-group">
                                  {{Form::label('password', 'Password', ["class" => "sr-only"])}}
                                  {{Form::password('password', ["class"=>"form-control", "placeholder"=>"Password", "required"=>"" ])}}
                              </div>

                              <div class="form-group">
                                  {{Form::submit('Sign In', array("class" => "btn btn-primary btn-block"))}}
                              </div>
                              {{Form::close()}}
                          </div>
                          <div class="bottom text-center">
                              New here ? {{ link_to_route('user.create', 'Join us') }}
                          </div>
                      </div>
                  </li>
              </ul>
          </li>
          @endif
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<div class="container-fluid">
    @if(Session::has('loginError'))
        <div class="row">
            <div class="col-md-12">
                <p class="text-danger pull-right">{{ Session::get('loginError') }}</p>
            </div>
        </div>
    @endif

    @yield('content')

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</body>
</html>
