@extends ("product.layout")
@section ("content")

<h2>Create new user</h2>
    {{ Form::open(array('action' => array('UserController@store'))) }}
        <div class="form-group">
            {{Form::label('username', 'Username')}}
            {{Form::text('username')}}
            <div class="help-block with-errors">{{$errors->first('username')}}</div>
        </div>
        <div class="form-group">
            {{Form::label('password', 'Password')}}
            {{Form::password('password')}}
            <div class="help-block with-errors">{{$errors->first('password')}}</div>
        </div>
        {{Form::submit('Submit')}}

    {{Form::close()}}

@stop
@stop
