<?php

class UserController extends \BaseController {

    public function login(){

        $values = Input::all();
        if (Auth::attempt(array('username' => $values['username'], 'password' => $values['password']))){
            Session::put('loginError', "");
        }
        else {
            Session::put('loginError', "Username or Password Incorrect");
        }
        return Redirect::to(URL::previous());

    }

    public function logout(){
        Auth::logout();
        return Redirect::route("product.index");
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('user.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $values = Input::all();
        $v = Validator::make($values, User::$rules);
        if ( $v->passes()) {
            $user = new User();
            $user["username"] = $values["username"];
            $user["password"] = Hash::make($values["password"]);
            $user->save();
            //return Redirect::route("user.show", $user->id);
            return Redirect::route('product.index');
        }
        else
        {
            // Show validation errors
            return Redirect::action('UserController@create')->withErrors($v);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
