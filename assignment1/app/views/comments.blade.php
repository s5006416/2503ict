
@extends("layout")

@section('content')

    <div class="col-md-8 chat-window">
        <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <div class="col-md-8 col-xs-8">
                    <h3 class="panel-title" style="padding-bottom: 10px;"><span class="glyphicon glyphicon-comment"></span> Comments</h3>
                </div>
                <form action="{{{url("/createcomment/".$post->Id)}}}" method="get">
                    <div class="input-group" style="margin:10px 10px 0 10px; width: 50%">
                        <input id="nameInput" name="nameInput" class="form-control input-sm chat_input" placeholder="Enter your name..." type="text">
                    </div>
                    <div class="input-group" style="margin: 10px;">
                        <input id="messageInput" name="messageInput" class="form-control input-sm chat_input" placeholder="Write your comment here..." type="text">
                        <span class="input-group-btn">
                            <input type="submit" class="btn btn-primary btn-sm" id="btn-chat" value="Submit Comment">
                        </span>
                    </div>
                </form>
            </div>
            <div class="panel-body msg_container_base">


                <!-- ICON ON LEFT -->
                <div class="row msg_container base_receive">
                    <div class="col-md-2 col-xs-2 avatar">
                        <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                    </div>
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_receive">
                            <b>{{{$post->Title}}}</b>
                            <p>{{{$post->Content}}}</p>
                            <time datetime="2009-11-13T20:00">{{{$post->Name}}} • <span id="datespanPost">{{{$post->PostDate}}}</span>
                            </time>
                        </div>
                    </div>
                </div>
                <!-- This function will format the given date/time and place it in the element with the given id -->
                <script>formatLocalTime("{{{$post->PostDate}}}", "datespanPost");</script>
                @foreach($comments as $comment)
                        <!-- ICON ON RIGHT -->
                <div class="row msg_container base_sent">
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_sent">
                            <p>{{{$comment->Content}}}</p>
                            <time datetime="2009-11-13T20:00">{{{$comment->Name}}} • <span id="datespan{{{$post->Id}}}">{{{$comment->CommentDate}}}</span></time>
                            <a href="{{{url("/confirmdeletecomment/".$comment->Id)}}}" class="pull-left text-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-2 avatar">
                        <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                    </div>
                </div>
                <!-- This function will format the given date/time and place it in the element with the given id -->
                <script>formatLocalTime("{{{$post->PostDate}}}", "datespan{{{$post->Id}}}");</script>
                @endforeach
            </div>
            <!--div class="panel-footer">
                <div class="input-group">
                    <input id="btn-input" class="form-control input-sm chat_input" placeholder="Write your message here..." type="text">
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" id="btn-chat">Send</button>
                        </span>
                </div>
            </div-->
        </div>
    </div>

@stop
@stop
