@extends("layout")
@section('content')

    <div class="row">
        <div class="span12">
            <div class="hero-unit center">
                <h3>{{{$title}}}</h3>
                <br>
                <p>{{{$description}}}</p>
                <a href="{{{$confirmurl}}}" class="btn btn-large btn-danger">{{$oklabel}}</a>
                <a href="{{{$cancelurl}}}" class="btn btn-large btn-info">{{$cancellabel}}</a>
            </div>
            <br>
        </div>
    </div>
@stop
@stop
