
@extends("layout")

@section('content')
    <div class="col-md-8 chat-window">
        <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <div class="col-md-8 col-xs-8" style="padding-bottom: 10px;">
                <h3 class="panel-title"><span class="glyphicon glyphicon-edit"></span> Edit Post</h3>
                </div>
                <form action="{{{url("/submitpostedit/".$post->Id)}}}" method="get">
                    <div class="input-group" style="padding-bottom:10px;width:100%;">
                        <label for="titleInput">Title</label>
                        <input id="titleInput" name="titleInput" class="form-control input-sm chat_input" placeholder="Title your post..." type="text" value="{{{$post->Title}}}">
                    </div>
                    <div class="input-group" style="padding-bottom:10px;width:100%;">
                        <label for="messageInput">Post</label>
                        <textarea id="messageInput" name="messageInput" class="form-control input-sm chat_input" placeholder="Write your message here...">{{{$post->Content}}}</textarea>
                    </div>
                    <div class="input-group">
                    <input type="submit" value="Save Changes" class="btn btn-primary btn-sm" id="buttonSubmit">
                    <a href="{{url("/")}}" class="btn btn-default btn-sm" id="buttonCancel">Discard Changes</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@stop
