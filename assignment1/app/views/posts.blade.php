
@extends("layout")

@section('content')



    <div class="col-md-8 chat-window">
        <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <form action="{{{url('createpost')}}}" method="get">
                    <div class="input-group" style="padding-bottom:10px;">
                        <input id="nameInput" name="nameInput" class="form-control input-sm chat_input" placeholder="Enter your name..." type="text">
                    </div>
                    <div class="input-group" style="padding-bottom:10px;">
                        <input id="titleInput" name="titleInput" class="form-control input-sm chat_input" placeholder="Title your post..." type="text">
                    </div>
                    <div class="input-group">
                        <input id="messageInput" name="messageInput" class="form-control input-sm chat_input" placeholder="Write your message here..." type="text">
                        <span class="input-group-btn">
                            <input type="submit" value="Submit Post" class="btn btn-primary btn-sm" id="btn-chat">
                        </span>
                    </div>
                    <!--div class="col-md-8 col-xs-8">
                    <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> Chat - Miguel</h3>
                </div-->
                </form>
            </div>
            <div class="panel-body msg_container_base">
                @foreach($posts as $post)
                <!-- ICON ON RIGHT
                <div class="row msg_container base_sent">
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_sent">
                            <p>{{{$post->Content}}}</p>
                            <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-2 avatar">
                        <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                    </div>
                </div>
                -->

                <!-- ICON ON LEFT -->
                <div class="row msg_container base_receive">
                    <div class="col-md-2 col-xs-2 avatar">
                        <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                    </div>
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_receive">
                            <b>{{{$post->Title}}}</b>
                            <p>{{{$post->Content}}}</p>
                            <time datetime="{{{$post->PostDate}}}">{{{$post->Name}}} • <span id="datespan{{{$post->Id}}}">{{{$post->PostDate}}}</span> •
                                @if (count($comments[$post->Id]) == 1)
                                    {{ link_to_route('comments', count($comments[$post->Id]).' comment', [ $post->Id, ]); }}
                                @elseif (count($comments[$post->Id]))
                                    {{ link_to_route('comments', count($comments[$post->Id]).' comments', [ $post->Id, ]); }}
                                @else
                                    {{ link_to_route('comments', 'Add comment', [ $post->Id, ]); }}
                                @endif
                            </time>
                            <a href="{{{url("/confirmdeletepost/".$post->Id)}}}" class="pull-right text-danger"><i class="glyphicon glyphicon-remove"></i></a>
                            <span class="pull-right">&nbsp;&nbsp;&nbsp;</span>
                            <a href="{{{url("/editpost/".$post->Id)}}}" class="pull-right text-info"><i class="glyphicon glyphicon-edit"></i></a>
                        </div>
                    </div>
                </div>
                <!-- This function will format the given date/time and place it in the element with the given id -->
                <script>formatLocalTime("{{{$post->PostDate}}}", "datespan{{{$post->Id}}}");</script>
                @endforeach
            </div>
            <!--div class="panel-footer">
                <div class="input-group">
                    <input id="btn-input" class="form-control input-sm chat_input" placeholder="Write your message here..." type="text">
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" id="btn-chat">Send</button>
                        </span>
                </div>
            </div-->
        </div>
    </div>

@stop
@stop
