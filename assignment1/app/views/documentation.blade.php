
@extends("layout")

@section('content')

    <div class="col-md-8 chat-window">
        <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <div class="col-md-8 col-xs-8">
                    <h3 class="panel-title" style="padding-bottom: 10px;"><span class="glyphicon glyphicon-book"></span> Documentation</h3>
                </div>
            </div>
            <div class="panel-body doc_container_base">

                <h3>Project Documetation 2503ICT Assignment 1</h3>
                <h4>Mark Pointing S5006416</h2>
                <h4>ER Diagram for the Social Network Database</h4>
                <img src="/images/EER_Diagram.svg" alt="EER Diagram"/>
                <h4>Features Implemented</h4>
                <p>All required features were implemented, including the following:</p>
                <ul>
                    <li>Navigation menu on all pages</li>
                    <li>Create post form on home page</li>
                    <li>Each post contains an icon</li>
                    <li>Each post contains a title, message and name</li>
                    <li>Each post indicates the number of comments</li>
                    <li>Each post has edit and delete links</li>
                    <li>Each post has a view comments link which redirects to a new page</li>
                    <li>Each comment can be edited in a form on a new page</li>
                </ul>
                <h4>Challenges and Interesting Solutions</h4>
                    <p>The most difficult single aspect of this project was the implementation of date/time in the database.
                    Reading the documentation for sqlite3 revealed that there is no native date type for the database.
                    Instead sqlite3 implements dates as either a string or integer, and provides helper functions for the creation and conversion of dates.
                    These functions store dates as UTC time (GMT) in the database.  Getting local time in the local timezone on the client was
                    accomplished in javascript on the browser.</p>

                <h4>Extra Features</h4>
                    <p>All database delete operations redirect to a confirmation page before carrying out the delete operation.
                    This is designed to protect against inadvertent post or comment deletion.</p>
            </div>
        </div>
    </div>

@stop
@stop
