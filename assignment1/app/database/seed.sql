DROP TABLE IF EXISTS Posts;
DROP TABLE IF EXISTS Comments;
CREATE TABLE Posts (
  Id INTEGER PRIMARY KEY AUTOINCREMENT,
  PostDate DATE NOT NULL,
  Name VARCHAR(64) DEFAULT 'Anonymous' NOT NULL,
  Title VARCHAR(32) DEFAULT 'No Title!' NOT NULL,
  Content TEXT DEFAULT '' NOT NULL
);

CREATE TABLE Comments (
  Id INTEGER PRIMARY KEY AUTOINCREMENT,
  CommentDate DATE NOT NULL,
  Post INTEGER NOT NULL,
  Name VARCHAR(64) DEFAULT 'Anonymous' NOT NULL,
  Content TEXT DEFAULT '' NOT NULL
);

INSERT INTO Posts(Id, PostDate, Name, Title, Content)
VALUES
  (0, "2012-06-18 10:34:09", "Mario Bro", "Red Mushrooms Are Delicious.", "I really love the red mushrooms, but the only problem is they seem to make me put on a massive amount of weight."),
  (1, "2012-06-18 10:35:09", "Mario Bro", "Green Mushrooms Taste Funny", "The green mushrooms taste funny, but after I eat them, I feel like I have a whole new life.  It's really quite refreshing."),
  (2, "2012-06-18 10:36:09", "Mario Bro", "Peach is looking GOOD!", "Have you seen princess peach lately?  She is looking HOT! OMG Giggity Giggity."),
  (3, "2012-06-18 10:37:09", "Mario Bro", "Red Mushrooms Are Delicious.", "I really love the red mushrooms, but the only problem is they seem to make me put on a massive amount of weight."),
  (4, "2012-06-18 10:38:09", "Mario Bro", "Green Mushrooms Taste Funny", "The green mushrooms taste funny, but after I eat them, I feel like I have a whole new life.  It's really quite refreshing."),
  (5, "2012-06-18 10:39:09", "Mario Bro", "Peach is looking GOOD!", "Have you seen princess peach lately?  She is looking HOT! OMG Giggity Giggity.");

INSERT INTO Comments(Id, Post, CommentDate, Name, Content)
VALUES
  (NULL, 3, "2012-06-18 10:37:11", "Luigi Bro", "Yep the red ones defs make my clothes tighter."),
  (NULL, 4, "2012-06-18 10:38:12", "Luigi Bro", "I had a few green ones last week and ran into Koopa Troopa.  I thought it was game over, but thanks to the extra lives, he came off second best."),
  (NULL, 5, "2012-06-18 10:39:12", "Bowser Bowser", "Yeah, when I saw her yesterday I wanted to steal her and take her to my lava castle!"),
  (NULL, 5, "2012-06-18 10:39:29", "Princess Peach", "Well THANK YOU for noticing boys.");
