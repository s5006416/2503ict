<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* This is the root route.  It simply renders the home page using a query of all entries from 
the posts database, mashed with matching comments for the purposes of a count only*/
Route::get('/', function()
{
	// get all entries reverse ordered by date
	$posts = DB::select("SELECT Id,PostDate, Name, Title, Content FROM Posts ORDER BY PostDate DESC");
	$comments = array();
	// associate comments with each post
	foreach ($posts as $post)
	{
		$comments[$post->Id] = DB::select("SELECT * FROM comments WHERE Post = ?", array($post->Id));
	}
	// render view
	return View::make('posts')->withPosts($posts)->withComments($comments);
});

/* This route gets all the comments associated with a given post
and renders the comments view */
Route::get('/comments/{postId}', array('as'=>"comments", function($postId)
{
	
	$posts = DB::select("SELECT Id,PostDate, Name, Title, Content FROM Posts WHERE Id=? ORDER BY PostDate DESC", array($postId));
	$post = $posts[0];
	$comments = DB::select("SELECT * FROM comments WHERE Post = ?", array($post->Id));
	return View::make('comments')->withPost($post)->withComments($comments);
}));

/* This route creates a new post.  it does not display anything, but simply 
adds the post to the database, and then redirects back to the home page */
Route::get('/createpost', array('as'=>"createpost", function()
{
	// read the query parameters
	$name = $_GET["nameInput"];
	$title = $_GET["titleInput"];
	$message = $_GET["messageInput"];

	// redirect to an error page if any field is empty
	if (empty($name) || empty($title) || empty($message))
	{
		return View::make('errorpage')->withErrortitle("There was a problem with your post!")->withErrordescription('You need to enter a name, title, and message!')->withReturnurl(url("/"));
	}

	// perform the actual DB insertion
	DB::table('posts')->insert(
		[
			'PostDate' => date('Y-m-d H:i:s'),
			'Name' => $name,
			'Title' => $title,
			'Content' => $message
		]
	);
	
	// return to home page
	return Redirect::to(url("/"));
}));

/* This route redirects to the confirmation page.  It is used as confirmation of delete, which then 
redirects to the route which actually performs the redirect.*/
Route::get('/confirmdeletepost/{id}', array('as'=>"confirmdeletepost", function($id)
{
	// get the post in question so that we can display some information about it on the confirmation page
	$posts = DB::select("SELECT Id,PostDate, Name, Title, Content FROM Posts WHERE Id=? ORDER BY PostDate DESC", array($id));
	$post = $posts[0];
		return View::make('confirm')
			->withTitle("Are you sure you want to delete this post?")
			->withDescription($post->Name.': '.$post->Content)
			->withConfirmurl(url('/deletepost/'.($post->Id)))
			->withCancelurl(url('/'))
			->withOklabel('Delete')
			->withCancellabel('Cancel');
}));

/* This route performs the actual post deletion.  It removes the post from the
database, and then redirects to the home page */
Route::get('/deletepost/{id}', array('as'=>"deletepost", function($id)
{
	$sql = "delete from Posts where Id = ?";
	DB::delete($sql, array($id));
	$sql = "delete from Comments where Post = ?";
	DB::delete($sql, array($id));
	return Redirect::to(url("/"));
}));

/* This route loads a post and then renders the post edit form. 
The actual edit operation is performed on a different route */
Route::get('/editpost/{id}', array('as'=>"editpost", function($id)
{
	$posts = DB::select("SELECT Id,PostDate, Name, Title, Content FROM Posts WHERE Id=? ORDER BY PostDate DESC", array($id));
	$post = $posts[0];
	return View::make('edit')->withPost($post);
}));

/* This is the route where the edited post is submitted, after updating
the database, it redirects to the home page */
Route::get('/submitpostedit/{id}', array('as'=>"submitedit", function($id)
{

	$title = $_GET["titleInput"];
	$message = $_GET["messageInput"];

	// redirect to an error page if any field is empty
	if (empty($title) || empty($message))
	{
		return View::make('errorpage')->withErrortitle("There was a problem with your post!")->withErrordescription('You need to make sure title, and message are not blank!')->withReturnurl(url("/editpost/".$id));
	}
	
	$sql = "update Posts set Title=?, Content=? where id=?";
	DB::update($sql, array($title, $message, $id));
	
	return Redirect::to(url("/"));

}));

/* This route submits new comments to the database.  It is triggered from the
home page, and redirects back there after completion */
Route::get('/createcomment/{postId}', array('as'=>"createpost", function($postId)
{
	// read the query parameters
	$name = $_GET["nameInput"];
	$message = $_GET["messageInput"];

	// redirect to an error page if any field is empty
	if (empty($name) || empty($message))
	{
		return View::make('errorpage')
			->withErrortitle("There was a problem with your comment!")
			->withErrordescription('You need to enter your name, and a message!')
			->withReturnurl(url("/comments/".$postId));
	}

	// perform the actual db insertion
	DB::table('comments')->insert(
		[
			'CommentDate' => date('Y-m-d H:i:s'),
			'Post' => $postId,
			'Name' => $name,
			'Content' => $message
		]
	);
	
	// redirect to comments page.
	return Redirect::to(url("/comments/".$postId));
}));

/* This route redirects to the confirmation page for comment deletion,
the actual deletion is done on another page */
Route::get('/confirmdeletecomment/{id}', array('as'=>"confirmdeletecomment", function($id)
{
	// get the comment so we can display some information about it on the confirmation page.
	$comments = DB::select("SELECT Id, Name, Content, Post FROM Comments WHERE Id=?", array($id));
	$comment = $comments[0];
	return View::make('confirm')
		->withTitle("Are you sure you want to delete this comment?")
		->withDescription($comment->Name.': '.$comment->Content)
		->withConfirmurl(url('/deletecomment/'.($comment->Id)))
		->withCancelurl(url('/comments/'.$comment->Post))
		->withOklabel('Delete')
		->withCancellabel('Cancel');
}));

/* This route performs the actual comment deletion, and then redirects back to 
the comments page for the associated post*/
Route::get('/deletecomment/{id}', array('as'=>"deletecomment", function($id)
{
	$comments = DB::select("SELECT Post FROM Comments WHERE Id=?", array($id));
	$post = $comments[0]->Post;
	$sql = "delete from Comments where Id = ?";
	DB::delete($sql, array($id));
	return Redirect::to(url("/comments/".$post));
}));

/* Route to display the documentation */
Route::get('/documentation', array('as'=>"documentation", function()
{
	return View::make('documentation');
}));