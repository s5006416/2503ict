<?php

include "defs.php";

$number = "";

# Set $number to the value entered in the form.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $number = $_POST['number'];
}
else
{
    $number = "";
}
$error = "";

# Check $number is nonempty, numeric and between 2 and PHP_MAX_INT = 2^31-1.
# (PHP makes it difficult to do this naturally; see the manual.)
if (empty($number)) {
    $error = "Error: Missing value\n";
} else if (!is_numeric($number)) {
    $error = "Error: Nonnumeric value: $number\n";
} else if ($number < 2 || $number != strval(intval($number))) {
    $error = "Error: Invalid number: $number\n";
} else {
    # Set $factors to the array of factors of $number.
    $factors = factors($number);
    # Set $factors to a single dot-separated string of numbers in the array.
    $factors = join(" . ", $factors);
}

if (empty($error)) {
    $errclass = "";
}else{
    $errclass = "class=\"alert\"";
}


include('index_view.php');
?>


