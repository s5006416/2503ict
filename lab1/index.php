<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
    </head>
    <body>
        <h1>Hello World</h1>
        <h1>This is a heading</h1>
        <ul>
            <li>This</li>
            <li>is</li>
            <li>a</li>
            <li>list</li>
        </ul>
        <p> and here's a table:</p>
        <table>
          <tr>
            <th>First</td>
            <th>Last</td>
            <th>Age</td>
          </tr>
          <tr>
            <td>Mark</td>
            <td>Pointing</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Ronald</td>
            <td>McDonald</td>
            <td>72</td>
          </tr>
        </table> 
        <br/>
        <br/>
            <img src="/2503ict/lab1/image.jpg" alt="a cool dog picture"></img>
            <br/>
        <br/>
            <a href="second_page.php">Go To Second Page</a>
        <br/>
        <br/>
            <address>
            This awesome page is written by <a href="mailto:mark.pointing@griffithuni.edu.au">Mark Pointing</a>.<br>
            Visit me at:<br>
            <a href="www.futurepoint.com.au">futurepoint.com.au</a><br>
            or drop me a message at P.O. Box 775, Buckingham Palace<br>
            London, England 
            </address> 
        </body>
    </html>
