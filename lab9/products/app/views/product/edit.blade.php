@extends ("product.layout")
@section ("content")


    <h2>Edit product {{$product["id"]}}</h2>
    {{-- Form::model($product, array('action' => array('ProductController@update'))) --}}
    {{Form::model($product, array("method" => "PUT", "route" => array('product.update', $product['id'])))}}

        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name')}}
            <div class="help-block with-errors">{{$errors->first('name')}}</div>
        </div>
        <div class="form-group">
            {{Form::label('price', 'Price')}}
            {{Form::text('price')}}
            <div class="help-block with-errors">{{$errors->first('price')}}</div>
        </div>
        {{Form::submit('Submit')}}

    {{Form::close()}}





@stop
@stop
