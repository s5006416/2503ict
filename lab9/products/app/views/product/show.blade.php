@extends ("product.layout")
@section ("content")
    <h3>Product {{{$product["id"]}}}</h2>
    <h1>{{{$product["name"]}}}</h1>
    <h2>Price: ${{{number_format($product["price"],2)}}}</h2>
@stop
@stop
