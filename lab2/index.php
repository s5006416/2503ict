<?php
// set up post variables here for easier migration to database later on.
$user = [id=>1, firstName => "Bowser", lastName => "Bowser", avatar => "/2503ict/images/bowser.jpg"];
$koopa = [id=>2, firstName => "Koopa", lastName => "Troopa", avatar => "/2503ict/images/koopatroopa.jpg"];
$peach = [id=>3, firstName => "Princess", lastName => "Peach", avatar => "/2503ict/images/peach.jpg"];
$mario = [id=>4, firstName => "Mario", lastName => "Mario", avatar => "/2503ict/images/mario.png"];
$luigi = [id=>5, firstName => "Luigi", lastName => "Luigi", avatar => "/2503ict/images/luigi.png"];

$friends = [$koopa, $peach, $mario, $luigi];

$posts = [
    [from => $user, to => $peach, message => "I'm going to kidnap you Peach!"],
    [from => $user, to => $mario, message => "Don't even bother trying to stop me Mario!"],
    [from => $user, to => $luigi, message => "Tell Mario to stay out of this."],
    [from => $user, to => $koopa, message => "Hey Koopa, I need some help with a kidnapping!"],
];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mark Pointing 2503ICT</title>

    <!-- Bootstrap -->
    <link href="/2503ict/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">FriendFace</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Find Friends</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Friends</a></li>
                    <li><a href="#">Photos</a></li>
                    <li><a href="#">Login</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="container">

        <!-- friends sidebar -->
        <div class="col-sm-4">
        <?php foreach ($friends as $friend) { ?>
            <div class="thumbnail clearfix">
                <img src="<?= $friend["avatar"]?>" class="img-responsive img-circle pull-left" alt="user avatar" height="50" width="50">
                <div class="caption" class="pull-right">
                    <a href="/2503ict/lab2/index.php?messageRecipient=<?=$friend["id"]?>" <h3><?= $friend["firstName"]?></h3></a>
                </div>
                <?php
                if ($_GET['messageRecipient'] == $friend["id"]) {
                    ?>
                    <form role = "form">

                        <div class = "form-group">
                            <label for = "name">Message <?= $friend['firstName']?></label>
                            <textarea class = "form-control" rows = "3"></textarea>
                        </div>

                    </form>

                    <?php
                }
                ?>
            </div>
        <?php } ?>
        </div>

        <!-- your posts -->
        <div class="col-sm-8">
            <h2><?= $user["firstName"]?></h2>
            <?php
            $numposts = rand(1,10);
            ?>
            <p><?=$numposts?> posts today.</p>
            <?php
            for ($i = 0; $i<$numposts; $i++) {
                $thispost = $i % count($posts);
                ?>
                <div class="panel">
                    <div class="panel-body">
                    <img src="<?= $posts[$thispost]["from"]["avatar"]?>" class="img-responsive img-circle pull-left" alt="user avatar" height="50" width="50">
                    <p><?= $posts[$thispost]["message"]?></p>
                    <img src="<?= $posts[$thispost]["to"]["avatar"]?>" class="img-responsive img-circle pull-right" alt="user avatar" height="50" width="50">
                </div>
                </div>
            <?php } ?>
        </div>

    </div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/2503ict/js/bootstrap.min.js"></script>
</body>
</html>

